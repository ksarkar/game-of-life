#include<iostream>
#include<vector>
#include<unistd.h>
#include<fstream>
#include<cstdlib>


#define GRID_SIZE 25
using namespace std;

struct Node{
    int x,y;
    bool isLife;

    Node * next;
};

Node* insert(Node *& start, int x, int y, bool flag = true);
Node* find(Node *start, Node *f);
Node* find(Node *start, int x, int y, bool flag);
Node* findxy(Node *start, int x, int y);
void deleteN(Node *start, Node *d);
void deleteL(Node *&start);
void copy(Node *&copy, Node *start);
void deleteOneNode(Node *&d);

void updateCell(Node *&n_start, Node *start);
void updateNeighbors(Node *&start, Node *n);
void fillAllNeighbors(Node *&start);
void readGrid(Node *&start);
void printGrid(Node *start);
int liveNeighbor(Node *start, Node *n);
void displayLives(Node *start);


int main(int argc, char** argv)
{
    Node *start = new Node;
    start->next = NULL;

    readGrid(start);
    fillAllNeighbors(start);

    //printGrid(start);

    Node *n_start;
    copy(n_start,start);
    //printGrid(n_start);
    
    //while (1)
    {
        updateCell(n_start, start);
        copy(start, n_start);

        displayLives(start);
      //  sleep(5);
    }
    return 0;
}

void updateCell(Node *&n_start, Node *start)
{
    //for each L cell and its neighbors
    for (Node *n = start->next, *ns = n_start->next; n!= NULL; n = n->next, ns = ns->next)
    {
        int neighbor = liveNeighbor(start,n);
        //cout << n->x << " " << n->y << " " << neighbor <<endl;
        if (n->isLife)
        {
            //Death condition
            if (neighbor >3 || neighbor < 2)
            {
                ns->isLife = false;
             //printGrid(n_start);
            }
        }
        else
        {
            //reproduce?
            if (neighbor == 3)
            {
                ns->isLife = true;
                updateNeighbors(n_start,ns);
            }
        }
        //printGrid(n_start);
    }
}


int liveNeighbor(Node *start, Node *n)
{
    int i = n->x; 
    int j = n->y;

    int neighbor = 0;
    for (int ii = i-1; ii <= i+1; ii++)
    {
        for (int jj = j-1; jj <= j+1; jj++)
        {
            if ((ii == i) && (jj == j)) 
                continue;

            //for every neighbor
            Node * t = find(start, ii, jj, true);
            if (t != NULL)
            {
                neighbor ++;
            }
        }
    }
    return neighbor;
}

void fillAllNeighbors(Node *&start)
{
    for (Node *n = start->next; n != NULL; n = n->next)
        updateNeighbors(start, n);
}

void updateNeighbors(Node *&start, Node *n)
{
    int i = n->x; 
    int j = n->y;

    for (int ii = i-1; ii <= i+1; ii++)
    {
        for (int jj = j-1; jj <= j+1; jj++)
        {
            if ((ii == i) && (jj == j)) 
                continue;

            //for every neighbor
            Node * t = findxy(start, ii, jj);
            if (t == NULL)
            {
                t = insert(start, ii, jj, false);
            }
        }
    }
}

void readGrid(Node *&start)
{
    int x,y;
    while (cin >> x)
    {
        cin >> y;
        insert(start, x, y, true);
    }
}

Node *find(Node *start, Node *f)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        if ((n->x == f->x) && (n->y == f->y)) 
            return n;
    }
    return NULL;
}

Node* findxy(Node *start, int x, int y)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        if ((n->x == x) && (n->y == y)) 
            return n;
    }
    return NULL;
}

Node* find(Node *start, int x, int y, bool flag)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        if ((n->x == x) && (n->y == y) && (n->isLife==flag)) 
            return n;
    }
    return NULL;
}

void deleteN(Node *start, Node *d)
{
    Node *t = find(start, d);
    deleteOneNode(d);
}

void deleteOneNode(Node *&d)
{
    if (d == NULL) return;

    if (d->next == NULL)
    {
     //   Node *t = d;
        d = NULL;
       // free(t);
    }
    else 
    {
        //make copy
        Node *t = d->next;
        
        d->x = t->x;
        d->y = t->y;
        d->isLife = t->isLife;

        d->next = t->next;

        free(t);
    }
}

Node* insert(Node *&start, int x, int y, bool flag)
{

    Node *node = new Node;
    node->x = x;
    node->y = y;
    node->isLife = flag;


    node->next = start->next;
    start->next = node;
    return node;
}

void copy(Node *&copy, Node *start)
{
    //destroying the previous list
    deleteL(copy);

    copy = new Node;
    copy->next = NULL;

    Node *prev = copy;

    for (Node *n = start->next; n != NULL; prev = prev->next, n = n->next)
    {
        Node * newnode = new Node;
        newnode->x = n->x;
        newnode->y = n->y;
        newnode->isLife = n->isLife;
        newnode->next = NULL;

        prev->next = newnode;
    }

    //update the neighbours

}

void deleteL(Node *&start)
{
    Node *prev = start;

    for(Node *n = start->next; n != NULL; n = n->next)
    {
        free(prev);
        prev = n;
    }
}

void displayLives(Node *start)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        if (n->isLife)
            cout << n->x << " " << n->y << endl;
    }
}

void printGrid(Node *start)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        cout << n->x << "," << n->y << "\t" << n->isLife << "\t" << endl;
    }
}
