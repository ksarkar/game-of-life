#include<iostream>
#include<vector>
#include<unistd.h>

#define GRID_SIZE 25
using namespace std;

void printGrid(vector< vector<bool> > const &grid);
void fillGrid(vector< vector<bool> > &grid);
vector< vector<bool> > update(vector< vector<bool> > const &grid);
void updateCell(int i, int j, vector< vector<bool> > &ngrid, vector< vector<bool> > const &grid);

int main(int argc, char** argv)
{
    vector< vector<bool> > grid(GRID_SIZE, vector<bool>(GRID_SIZE, false));
    fillGrid(grid);

    vector< vector<bool> > ngrid;   //next grid
    while (1)
    {
        ngrid = update(grid);
        grid = ngrid;
        printGrid(grid);
        sleep(5);
    }
    return 0;
}

vector< vector<bool> > update(vector< vector<bool> > const &grid)
{
    vector< vector<bool> > ngrid = grid;
    
    //for each cell check and update
    for (int i = 1; i < ngrid.size()-1; i++)
    {
        for (int j = 1; j < ngrid[i].size()-1; j++)
        {
            updateCell(i ,j, ngrid, grid);
        }
    }

    return ngrid;
}

void updateCell(int i, int j, vector< vector<bool> > &ngrid, vector< vector<bool> > const &grid)
{
    int neighbor = 0;
    for (int ii = i-1; ii <= i+1; ii++)
    {
        for (int jj = j-1; jj <= j+1; jj++)
        {
            if((ii == i) && (jj == j)) 
                continue;
            if(grid[ii][jj] == true)
                neighbor++;
        }
    }

    //reproduce or death
    if (grid[i][j] == false)    //check for reproduction
    {
        if (neighbor == 3)  //reproduce
            ngrid[i][j] = true;
    }
    else                    //check for death
    {
        if ((neighbor > 3) || (neighbor < 2))
            ngrid[i][j] = false;    //DIE
    }
}

void fillGrid(vector< vector<bool> > &grid)
{
    int x,y;
    while (cin >> x)
    {
        cin >> y;
        
        grid[x][y] = true;
    }
}

void printGrid(vector< vector<bool> > const &grid)
{
    for (int i = 0; i < grid.size(); i++)
    {
        for (int j = 0; j < grid[i].size(); j++)
        {
            cout << ((grid[i][j] == true)? 'x': ' ');
        }
        cout << endl;
    }
    cout << endl << endl;
}
