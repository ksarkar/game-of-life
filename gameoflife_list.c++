#include<iostream>
#include<vector>
#include<unistd.h>
#include<fstream>

#define GRID_SIZE 25
using namespace std;

struct Node{
    int x,y;
    bool isLife;

    vector<Node *> neighbors;
    Node * next;
};

Node* insert(Node *& start, int x, int y, bool flag = true);
Node* find(Node *start, Node *f);
Node* find(Node *start, int x, int y);

void updateNeighbors(Node *&start, Node *n);
void fillAllNeighbors(Node *&start);
void readGrid(Node *&start);
void printGrid(Node *start);


int main(int argc, char** argv)
{
    Node *start = new Node;
    start->next = NULL;

    readGrid(start);
    fillAllNeighbors(start);

    printGrid(start);

    Node *n_start;
    copy(n_start,start);
    while (1)
    {
        ngrid = update(grid);
        grid = ngrid;
        printGrid(grid);
        sleep(5);
    }

    return 0;
}


void fillAllNeighbors(Node *&start)
{
    for (Node *n = start->next; n != NULL; n = n->next)
        updateNeighbors(start, n);
}

void updateNeighbors(Node *&start, Node *n)
{
    int i = n->x; 
    int j = n->y;

    for (int ii = i-1; ii <= i+1; ii++)
    {
        for (int jj = j-1; jj <= j+1; jj++)
        {
            if ((ii == i) && (jj == j)) 
                continue;

            //for every neighbor
            Node * t = find(start, ii, jj);
            if (t == NULL)
            {
                t = insert(start, ii, jj, false);
            }
            n->neighbors.push_back(t);
        }
    }
}

Node *find(Node *start, Node *f)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        if ((n->x == f->x) && (n->y == f->y)) 
            return n;
    }
    return NULL;
}

Node* find(Node *start, int x, int y)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        if ((n->x == x) && (n->y == y)) 
            return n;
    }
    return NULL;
}

void readGrid(Node *&start)
{
    int x,y;
    while (cin >> x)
    {
        cin >> y;
        insert(start, x, y, true);
    }
}

Node* insert(Node *&start, int x, int y, bool flag)
{

    Node *node = new Node;
    node->x = x;
    node->y = y;
    node->isLife = flag;


    node->next = start->next;
    start->next = node;
    return node;
}
/*
void copy(Node *&copy, Node *start)
{
    Node *copy = new Node;
    copy->next = NULL;

    Node *prev = copy;

    for (Node *n = start->next; n != NULL; n = n->next)
    {
        Node * newnode = new Node;
        newnode->x = n->x;
        newnode->y = n->y;
        newnode->isLife = n->isLife;
        newnode->next = NULL;

        prev->next = newnode;
    }

    //update the neighbours

}
*/
void updateLinks(Node *&start)
{
    for (Node *i = start->next; i != NULL; i = i->next)
    {
        for (Node *j = start->next; j != NULL; j = j->next)
        {
            if (isNeighbor(i, j))
                j->neighbors.push_back(j);
        }
    }
}

        
void printGrid(Node *start)
{
    for (Node *n = start->next; n != NULL; n = n->next)
    {
        cout << n->x << "\t" << n->y << "\t" << n->isLife << "\t" << n->neighbors.size() << endl;
    }
}
